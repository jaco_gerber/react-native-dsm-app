import axios from 'axios'
import base64 from 'react-native-base64'

const AxiosInstance = axios.create({
    baseURL: 'https://192.168.1.5:8443',
    timeout: 10000,
    headers: {
        'Content-Type': 'application/json'
    }
})

import { NativeModules } from 'react-native';
const { DSM } = NativeModules;

export const registerDevice = async (device_uid, project_key, device_sign_key, device_enc_key, os_type, os_version, device_model) => {

    console.log(device_uid, project_key, device_sign_key, device_enc_key, os_type, os_version, device_model)
    var lines = device_sign_key.split('\n')
    const dev_sign_key= lines.join('')

    const body = {
        device_uid,
        project_key,
        device_sign_key: dev_sign_key,
        device_enc_key,
        os_type,
        os_version,
        device_model
    }
    console.log(body)

    // const res = await AxiosInstance.post(
    //     `/api/v1/devices`,
    //     body
    // )
    // // .then(response => {
    // //     console.log(response)
    // //     return response
    // // })
    // // .catch(error => {
    // //     console.log(response)
    // //     return error
    // // })
    // console.log(res)
    // return res
}

export const registerCustomer = async (msisdn, lastname, name, idnumber, gender, dob, device_uid) => {
    let device_sign_priv_key
    await KeyStore.get('my.app.name.RSA.pvt')
        .then(res => {
            device_sign_priv_key = res
        })
        .catch(err => {
            console.warn(err)
        })
    const deviceUidBase64 = base64.encode(device_uid + 'com.wallet.kawena.new')
    msisdn = "27849210430"
    lastname = "Gerber"
    name = "Jaco"
    idnumber = "8412025035089"
    gender = "M"
    dob = "1984-12-02"
    const signkey = device_sign_priv_key
    console.log(msisdn, lastname, name, idnumber, gender, dob)
    const enclastname = lastname
    const encname = name
    const encidnumber = idnumber
    console.log(msisdn, enclastname, encname, encidnumber, gender, dob)

    // 1. Get Body Params
    const body = {
        subs_id: 1000003,
        msisdn: msisdn,
        wallet_types: [1],
        fee_profile: "true",
        send_sms: true,
        party: {
            party_id: 0,
            party_class: 0,
            person: {
                lastname: enclastname,
                name: encname,
                id_number: encidnumber,
                gender: gender,
                date_of_birth: dob
            }
        }
    }
    console.log('body', body)

    // 2. Iclude Query Params If Any
    const params = ''

    // 3. Method String
    const methodStr = "POST"
    console.log('methodStr', methodStr)

    // 4. Create Body Hash binary_to_list(base64:encode(crypto:hash(sha256,Body)))
    let bodyHash 
    DSM.hashData(body, (err) => {
        console.log(err)
      }, (msg) => {
        console.log(msg)
        bodyHash=msg
      })
    // console.log('bodyHash', bodyHash)

    // 5. Build MACData
    const ts = Math.round((new Date).getTime() / 1000)
    const macData = methodStr + ' ' + '/api/v1/channel/customers' + params + " HTTP/1.1\n" + ts + "\n" + bodyHash
    console.log('macData', macData)

    let signature
    DSM.signData(macData, (err) => {
        console.log(err)
      }, (msg) => {
        console.log(msg)
        signature=msg
      })
    
        console.log('signature',signature)

    const options = {
        headers: {
            "Authorization": `DEVICE id="${deviceUidBase64}", ts="${ts}", h="X-Body-Hash", signature="${signature}"`,
            "X-Body-Hash": bodyHash
        }
    }

    console.log(options)
    console.log(body)

    // const res = await AxiosInstance.post(
    //     `/api/v1/channel/customers`,
    //     body,
    //     options
    // )
    //     .then(response => {
    //         console.log(response)
    //         return response
    //     })
    //     .catch(error => {
    //         console.log(error)
    //         return error
    //     })
    // console.log(res)
    // return res
}

export const linkDevice = (accountNumber, device_uid, bodyHash, access_token) => {
    console.log(accountNumber, device_uid, bodyHash)
    const params = ''
    const methodStr = "POST"
    const ts = 1599481499

    const macData = methodStr + ' ' + '/api/v1/channel/customers/' + accountNumber + '/devices' + params + " HTTP/1.1\n" + ts + "\n" + bodyHash

    console.log(macData)

    let signature 
    
    DSM.signData(macData, (err) => {
        console.log(err)
      }, (msg) => {
        console.log(msg)
        signature=msg
      })

    const options = {
        headers: {
            "Authorization": `DEVICE id=${device_uid}, ts=${ts}, h=X-Body-Hash, signature=${signature}, access_token=${access_token}`
        }
    }

    console.log(options)

    // const res = await AxiosInstance.post(
    //     `/api/v1/channel/customers/27849210431/devices`,
    //     body,
    //     options
    // )
    //     .then(response => {
    //         console.log(response)
    //         return response
    //     })
    //     .catch(error => {
    //         console.log(error)
    //         return error
    //     })
    // console.log(res)
    // return res
}

export const activateDevice = (accountNumber, device_uid, bodyHash, access_token) => {
    const params = ''
    const methodStr = "PUT"
    const ts = 1599481499

    const macData = methodStr + ' ' + '/api/v1/channel/customers/' + accountNumber + '/devices/' + device_uid.replace(/(\r\n|\n|\r)/gm,"") + '/activate' + params + " HTTP/1.1\n" + ts + "\n" + bodyHash

    console.log(macData)

    let signature 
    
    DSM.signData(macData, (err) => {
        console.log(err)
      }, (msg) => {
        console.log(msg)
        signature=msg
      })

    const options = {
        headers: {
            "Authorization": `DEVICE id=${device_uid}, ts=${ts}, h=X-Body-Hash, signature=${signature}, access_token=${access_token}`
        }
    }

    console.log(options)

    // const res = await AxiosInstance.post(
    //     `/api/v1/channel/customers/27849210431/devices`,
    //     body,
    //     options
    // )
    //     .then(response => {
    //         console.log(response)
    //         return response
    //     })
    //     .catch(error => {
    //         console.log(error)
    //         return error
    //     })
    // console.log(res)
    // return res
}
