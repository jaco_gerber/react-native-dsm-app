/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { NativeModules } from 'react-native';
const { DSM } = NativeModules;

import * as device from './src/components/Device'
import base64 from 'react-native-base64'

const App: () => React$Node = () => {
  const [deviceUid, setDeviceUid] = React.useState('')
  const [osType, setOsType] = React.useState('')
  const [osVersion, setOsVersion] = React.useState('')
  const [deviceModel, setDeviceModel] = React.useState('')
  const [deviceSignPublicKey, setDeviceSignPublicKey] = React.useState('')
  const [deviceEcPublicKey, setDeviceEcPublicKey] = React.useState('')
  
  const [signData, setSignData] = React.useState('')
  const [signDataSignature, setSignDataSignature] = React.useState('')
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <Button onPress={() => { DSM.showToast('This is a native toast!!') }} title="Toast Btn" />
          <Button onPress={() => {
            DSM.generateKeys((err) => {
              console.log(err)
            }, (msg) => {
              console.log(msg)
            })
          }}
            title="generateKeys"
          />
          <Button onPress={() => {
            DSM.restoreKeys((err) => {
              console.log(err)
            }, (msg) => {
              console.log(msg)
            })
          }}
            title="restoreKeys"
          />
          <Button onPress={() => {
            DSM.getServerSignPubKey((err) => {
              console.log(err)
            }, (msg) => {
              console.log(msg)
            })
          }}
            title="getServerSignPubKey"
          />
          <Button onPress={() => {
            DSM.getServerEncPubKey((err) => {
              console.log(err)
            }, (msg) => {
              console.log(msg)
            })
          }}
            title="getServerEncPubKey"
          />
          <Button onPress={() => {
            DSM.getDeviceSignPubKey((err) => {
              console.log(err)
            }, (msg) => {
              console.log(msg)
              setDeviceSignPublicKey(msg)
            })
          }}
            title="getDeviceSignPubKey"
          />
          <Button onPress={() => {
            DSM.getDeviceEncPubKey((err) => {
              console.log(err)
            }, (msg) => {
              console.log(msg)
            })
          }}
            title="getDeviceEncPubKey"
          />
          <Button onPress={() => {
            DSM.getDeviceID((err) => {
              console.log(err)
            }, (msg) => {
              console.log(msg)
              setDeviceUid(msg)
              setOsType(Platform.OS === 'iOs' ? 'IOS' : 'ANDROID')
              setOsVersion('7.0')
              setDeviceModel('Android SDK built for x86')
            })
          }}
            title="getDeviceID"
          />
          <Button onPress={() => {
            DSM.storeServerKeys(
              "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAps/RVQF0QFuU9oCfx4w9X10RnrohPkrqjQe2GFwymc2Ay0D8Y7Vxv8WR5CBdwrlcdthc9cvp7yZYs5fqxoXjn4B1glWjHjM/Ow2EqDTE3THi3fwXJBel53f8u7Iunm++zWtHzVrlEazRdyFNLfRMtNgNlVRS2yN6OX6zn8/7H7WDie5W5z+ehcjJGuQXL5euuXZaYaI4acbIgGAICLcboxX7efpnIAbfi6c16K1QFlD6Bi+ZS1B24RmmSCDZhwbjZIpWtQeHH2XgKJMAmBq9Wod1GUEFRa8WxZmLUGpP1OPuowr32no8KNDqfZKlpUIbHJPtvjsnHOIa5Z1PrR7ixwIDAQAB",
              "BESHNQYn3LkSs9BVraDrXxSw6Sx4S15cWWI1kRekIvIwdZ91Wzms5gDjFzQWnIsnJG9cClPNrFlHVY2ZZWxD0ME=",
              (err) => {
                console.log(err)
              }, (msg) => {
                console.log(msg)

              })
          }}
            title="storeServerKeys"
          />
          <Button onPress={() => {
            setSignData('POST /api/v1/channel/customers/27849210431/devices HTTP/1.1\n1599481499\nlQDdVFeQYMmvrXT09BQ1tlmanF5XLGaqGvzjV0UTuLU=')

            DSM.signData(signData, (err) => {
              console.log(err)
            }, (msg) => {
              console.log(msg)
              setSignDataSignature(msg)
            })
          }}
            title="signData"
          />
          <Button onPress={() => {
            console.log(signData, signDataSignature)
            DSM.verifyDeviceSignature(signData, signDataSignature, (err) => {
                console.log(err)
              }, (msg) => {
                console.log(msg)
              })
          }}
            title="verifyDeviceSignature"
          />
          <Button onPress={() => {
            DSM.hashData("{\r\n  \"verification_token\": \"093F19777C55B210\",\r\n  \"otp\": \"QkErbmIrWko4NU5oNUdmVy85akxBMGxYOWFWMjhsYlpXZHQxblBiNWdoUzNuZ3VFWitsSGtoc2VkdFNiNGh2Vzl3NnBvN09kdmVobQpKb3hSZjBtdzBVTmplTmJISHp2b2djVzU4VXdaQkIvVkx4RklpNldIVHh6YlJWZTduT0NDLy80VUpUUT0=\"\r\n}", (err) => {
              console.log(err)
            }, (msg) => {
              console.log(msg)
            })
          }}
            title="hashData"
          />
          <Button onPress={() => {
            DSM.encrypt("99005", (err) => {
              console.log(err)
            }, (msg) => {
              console.log(msg)
              console.log(base64.encode(msg))
            })
          }}
            title="encrypt"
          />
          <Button onPress={() => {
            DSM.decrypt("", (err) => {
              console.log(err)
            }, (msg) => {
              console.log(msg)
            })
          }}
            title="decrypt"
          />
          <Button onPress={() => {
            DSM.getDeviceSignPrivKey((err) => {
              console.log(err)
            }, (msg) => {
              console.log(msg)
            })
          }}
            title="getDeviceSignPrivKey"
          />
                    <Button
            onPress={() => {
              device.registerDevice(
                deviceUid,
                'KEY1',
                deviceSignPublicKey,
                deviceEcPublicKey,
                osType,
                osVersion,
                deviceModel
              )

            }}
            title='1. Register device'
            color='#A0A0A0'
            accessibilityLabel='1. DSM Register device'
          />
          <Button
            onPress={() => {
              device.linkDevice(
                '27849210431',
                deviceUid,
                "lQDdVFeQYMmvrXT09BQ1tlmanF5XLGaqGvzjV0UTuLU=",
                "LKCH7wMH8w14/CpLf4rwYif6ax8uotsdckGn6gnKIIs="
              )
            }}
            title='linkDevice'
            color='#841584'
            accessibilityLabel='linkDevice'
          />
          <Button
            onPress={() => {
              device.activateDevice(
                '27849210431',
                deviceUid,
                "ijlLv+vBSEe7OITZDWTfsCPd91YL4bGYsGWNIb8cL8s=",
                "EDB727738813BE23DE3E58E6D6F760BB"
              )
            }}
            title='activateDevice'
            color='#841584'
            accessibilityLabel='activateDevice'
          />
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
