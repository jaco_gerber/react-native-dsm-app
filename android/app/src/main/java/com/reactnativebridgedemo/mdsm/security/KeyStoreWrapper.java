package mdsm.security;

import android.content.Context;
import android.security.KeyPairGeneratorSpec;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableKeyException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Calendar;
import java.util.Date;

import org.spongycastle.util.io.pem.PemObject;
import org.spongycastle.util.io.pem.PemReader;
import org.spongycastle.util.io.pem.PemWriter;

import javax.crypto.Cipher;
import javax.security.auth.x500.X500Principal;

public class KeyStoreWrapper {
    private KeyStore keyStore;
    private Context context;

    private static final String KEYSTORE_PROVIDER = "AndroidKeyStore";

    public static final String ALGORITHM_RSA = "RSA";

    public KeyStoreWrapper(Context context) throws KeyStoreException {
        try {
            this.context = context;
            this.keyStore = KeyStore.getInstance(KEYSTORE_PROVIDER);
            this.keyStore.load(null);
        } catch (GeneralSecurityException | IOException e) {
            throw new KeyStoreException(e);
        }
    }

    public boolean hasAlias(String alias) throws GeneralSecurityException {
        try {
            return this.keyStore.containsAlias(alias);
        } catch (KeyStoreException e) {
            throw new GeneralSecurityException(e);
        }
    }

    public PublicKey getPublicKey(String alias) throws GeneralSecurityException {
        try {
            return this.keyStore.getCertificate(alias).getPublicKey();
        } catch (KeyStoreException e) {
            throw new GeneralSecurityException(e);
        }
    }

    public PrivateKey getPrivateKey(String alias) throws UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException {
        System.out.println("**J**A**V**A** -KeyStoreWrapper-  -getPrivateKey- ");
        System.out.println("**J**A**V**A** -KeyStoreWrapper-  -getPrivateKey- -alias- " + alias);
        System.out.println("**J**A**V**A** -KeyStoreWrapper-  -getPrivateKey- -keyStore.getKey()- " + keyStore.getKey(alias, null));
        return (PrivateKey) keyStore.getKey(alias, null);
    }

    public KeyPair generateKeys(String algorithm, String alias, int keySize) throws GeneralSecurityException {
        KeyPairGenerator kpg = KeyPairGenerator.getInstance(algorithm, KEYSTORE_PROVIDER);

        Calendar cal = Calendar.getInstance();
        Date startDate = cal.getTime();
        cal.add(Calendar.YEAR, 999);
        Date endDate = cal.getTime();

        kpg.initialize(new KeyPairGeneratorSpec.Builder(context).
                setAlias(alias).
                setSubject(new X500Principal("CN=" + alias)).
                setSerialNumber(BigInteger.ONE).
                setStartDate(startDate).
                setEndDate(endDate).
                setKeyType(algorithm).
                setKeySize(keySize).
                build());
        return kpg.generateKeyPair();
    }

    public byte[] rsaEncrypt(String alias, byte[] data) throws GeneralSecurityException {
        Cipher instance = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        instance.init(Cipher.ENCRYPT_MODE, getPublicKey(alias));
        return instance.doFinal(data, 0, data.length);
    }

    public byte[] rsaDecrypt(String alias, byte[] data) throws GeneralSecurityException {
        PrivateKey keyStorePrivateKey = (PrivateKey) keyStore.getKey(alias, null);
        Cipher instance = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        instance.init(Cipher.DECRYPT_MODE, keyStorePrivateKey);
        return instance.doFinal(data, 0, data.length);
    }

    public PublicKey loadPublicKeyFromFile(String filename, String keyAlgorithm) throws GeneralSecurityException {
        byte[] data = loadDataFromFile(filename);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(data);
        KeyFactory keyFactory = KeyFactory.getInstance(keyAlgorithm);
        return keyFactory.generatePublic(keySpec);
    }

    public void savePublicKey(PublicKey publicKey, String filename) throws GeneralSecurityException {
        saveDataToFile(publicKey.getEncoded(), filename);
    }

    public PrivateKey loadPrivateKeyFromFile(String filename, String keyAlias, String keyAlgorithm) throws GeneralSecurityException, IOException {
        File file = new File(context.getFilesDir(), filename);
        if (!file.exists())
            return null;

        PemReader pemReader = new PemReader(new InputStreamReader(new FileInputStream(file)));
        PemObject pemObject = pemReader.readPemObject();

        KeyFactory keyFactory = KeyFactory.getInstance("EC", "SC");
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(pemObject.getContent());
        return keyFactory.generatePrivate(keySpec);
    }

    public void savePrivateKeyToFile(PrivateKey privateKey, String keyAlias, String filename) throws IOException {
        PemObject pemObject = new PemObject(keyAlias, privateKey.getEncoded());
        File file = new File(context.getFilesDir(), filename);
        PemWriter pemWriter = new PemWriter(new OutputStreamWriter(new FileOutputStream(file)));
        try {
            pemWriter.writeObject(pemObject);
        } finally {
            pemWriter.close();
        }
    }

    public void savePublicKeyToFile(PublicKey publicKey, String keyAlias, String filename) throws IOException {
        PemObject pemObject = new PemObject(keyAlias, publicKey.getEncoded());
        File file = new File(context.getFilesDir(), filename);
        PemWriter pemWriter = new PemWriter(new OutputStreamWriter(new FileOutputStream(file)));
        try {
            pemWriter.writeObject(pemObject);
        } finally {
            pemWriter.close();
        }
    }

    private void saveDataToFile(byte[] data, String filename) throws GeneralSecurityException {
        try {
            File file = new File(context.getFilesDir(), filename);
            try (FileOutputStream fos = new FileOutputStream(file)) {
                fos.write(data);
            }
        } catch (IOException e) {
            throw new GeneralSecurityException(e);
        }
    }

    private byte[] loadDataFromFile(String filename) throws GeneralSecurityException {
        File file = new File(context.getFilesDir(), filename);
        try {
            FileInputStream fis = new FileInputStream(file);
            try {
                byte[] data = new byte[(int) file.length()];
                fis.read(data, 0, data.length);
                return data;
            } finally {
                fis.close();
            }
        } catch (Exception e) {
            throw new GeneralSecurityException(e);
        }
    }
}
