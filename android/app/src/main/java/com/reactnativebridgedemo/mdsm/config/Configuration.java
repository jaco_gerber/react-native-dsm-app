package mdsm.config;

import mdsm.security.ECIES;

public class Configuration {
    private final String encECCurveName;
    private String host;
    private String projectKey;
    private int certExpiryMonths;
    private String signingCertSubject;
    private String signKeystoreAlias;
    private int signKeySize;
    private String encPubKeyFilename;
    private String encPrivKeyFilename;
    private String serverSignPubKeyFilename;
    private String serverEncPubKeyFilename;
    private boolean autoAckMessage;

    public Configuration() {
        this.host = "http://192.168.8.109:7790";
        this.projectKey = "1";
        this.certExpiryMonths = 1200; // Default to 100 years
        this.signingCertSubject = null; // Default to KeyStore alias
        this.signKeySize = 2048;
        this.signKeystoreAlias = "mdsm.device.signing.key";
        this.encPrivKeyFilename = "mdsm.device.enc.priv";
        this.encPubKeyFilename = "mdsm.device.enc.pub";
        this.serverSignPubKeyFilename = "mdsm.server.sign.pub";
        this.serverEncPubKeyFilename = "mdsm.server.enc.pub";
        this.encECCurveName = ECIES.CURVE_SECP256K1;
    }

    public void setCertExpiryMonths(int certExpiryMonths) {
        this.certExpiryMonths = certExpiryMonths;
    }

    public void setSigningCertSubject(String signingCertSubject) {
        this.signingCertSubject = signingCertSubject;
    }

    public String getHost() {
        return host;
    }

    public int getCertExpiryMonths() {
        return certExpiryMonths;
    }

    public String getSigningCertSubject() {
        return signingCertSubject;
    }

    public String getServerSignPubKeyFilename() {
        return serverSignPubKeyFilename;
    }

    public String getServerEncPubKeyFilename() {
        return serverEncPubKeyFilename;
    }

    public void setSignKeystoreAlias(String signKeystoreAlias) {
        this.signKeystoreAlias = signKeystoreAlias;
    }

    public String getSignKeystoreAlias() {
        return signKeystoreAlias;
    }

    public String getEncPrivKeyFilename() {
        return encPrivKeyFilename;
    }
    
    public String getEncPubKeyFilename() {
        return encPubKeyFilename;
    }

    public void setEncPrivKeyFilename(String encPrivKeyFilename) {
        this.encPrivKeyFilename = encPrivKeyFilename;
    }

    public void setEncPubKeyFilename(String encPubKeyFilename) {
        this.encPubKeyFilename = encPubKeyFilename;
    }

    public boolean isAutoAckMessage() {
        return autoAckMessage;
    }

    public void setAutoAckMessage(boolean autoAckMessage) {
        this.autoAckMessage = autoAckMessage;
    }

    public void setProjectKey(String projectKey) {
        this.projectKey = projectKey;
    }

    public String getProjectKey() {
        return projectKey;
    }

    public String getEncECCurveName() {
        return encECCurveName;
    }

    public int getSignKeySize() {
        return signKeySize;
    }
    public void setSignKeySize(int keySize) {
        this.signKeySize = keySize;
    }
}
