package mdsm.security;

import org.spongycastle.jce.spec.IESParameterSpec;

import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.spec.ECGenParameterSpec;

import javax.crypto.Cipher;

public class ECIES
{
    public static String CURVE_SECP256K1 = "secp256k1";

    static
    {
        Security.addProvider(new org.spongycastle.jce.provider.BouncyCastleProvider());
    }

    public static KeyPair generateKeys(String curveName) throws NoSuchProviderException, NoSuchAlgorithmException, InvalidAlgorithmParameterException
    {
        KeyPairGenerator kpg = KeyPairGenerator.getInstance("EC", "SC");
        kpg.initialize(new ECGenParameterSpec(curveName));
        return kpg.generateKeyPair();
    }

    public static byte[] encrypt(PublicKey pubKey, byte[] data) throws GeneralSecurityException
    {
        Cipher iesCipher = Cipher.getInstance("ECIESwithAES-CBC", "SC");
        byte[] IVdata = new byte[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        iesCipher.init(Cipher.ENCRYPT_MODE, pubKey,
                new IESParameterSpec(null, null, 128, 128, IVdata));
        return iesCipher.doFinal(data, 0, data.length);
    }

    public static byte[] decrypt(PrivateKey privKey, byte[] data) throws GeneralSecurityException
    {
        Cipher iesCipher = Cipher.getInstance("ECIESwithAES-CBC", "SC");
        byte[] IVdata = new byte[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        iesCipher.init(Cipher.DECRYPT_MODE, privKey,
                new IESParameterSpec(null, null, 128, 128, IVdata));
        return iesCipher.doFinal(data, 0, data.length);
    }

}
