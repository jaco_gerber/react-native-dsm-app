package mdsm;

import android.content.Context;
import android.util.Base64;

import mdsm.config.Configuration;
import mdsm.security.ECIES;
import mdsm.security.Hash;
import mdsm.security.KeyStoreWrapper;
import mdsm.security.RSA;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;

public class CryptoSDK {
    private Context context;
    private Configuration config;
    private String deviceID;
    private PublicKey deviceSignPubKey;
    private PrivateKey deviceEncPrivKey;
    private PublicKey deviceEncPubKey;
    private PublicKey serverSignPubKey;
    private PublicKey serverEncPubKey;

    public CryptoSDK(Context context, Configuration config) {
        this.context = context;
        this.config = config;
        Security.addProvider(new org.spongycastle.jce.provider.BouncyCastleProvider());
    }

    public void generateKeys() {
        try {
            final KeyStoreWrapper keyStoreWrapper = new KeyStoreWrapper(context);
            if (!keyStoreWrapper.hasAlias(config.getSignKeystoreAlias())) {
                KeyPair deviceSignKeyPair = keyStoreWrapper.generateKeys("RSA", config.getSignKeystoreAlias(), 2048);
                this.deviceSignPubKey = deviceSignKeyPair.getPublic();
                this.deviceID = Base64.encodeToString(Hash.sha256(this.deviceSignPubKey.getEncoded()), Base64.URL_SAFE);
            }
            KeyPair encKeyPair = ECIES.generateKeys(config.getEncECCurveName());
            this.deviceEncPubKey = encKeyPair.getPublic();
            keyStoreWrapper.savePublicKeyToFile(this.deviceEncPubKey, config.getSignKeystoreAlias(), config.getEncPubKeyFilename());
            this.deviceEncPrivKey = encKeyPair.getPrivate();
            keyStoreWrapper.savePrivateKeyToFile(this.deviceEncPrivKey, config.getSignKeystoreAlias(), config.getEncPrivKeyFilename());
        } catch (GeneralSecurityException | IOException e) {
            throw (new RuntimeException(e));
        }
    }

    public void storeServerKeys(PublicKey serverSignPubKey, PublicKey serverEncPubKey) throws GeneralSecurityException {
        final KeyStoreWrapper keyStoreWrapper = new KeyStoreWrapper(context);
        keyStoreWrapper.savePublicKey(serverSignPubKey, config.getServerSignPubKeyFilename());
        keyStoreWrapper.savePublicKey(serverEncPubKey, config.getServerEncPubKeyFilename());
        this.serverSignPubKey = serverSignPubKey;
        this.serverEncPubKey = serverEncPubKey;
    }

    public void restoreKeys() {
        try {
            final KeyStoreWrapper keyStoreWrapper = new KeyStoreWrapper(context);
            if (keyStoreWrapper.hasAlias(config.getSignKeystoreAlias())) {
                this.deviceSignPubKey = keyStoreWrapper.getPublicKey(config.getSignKeystoreAlias());
                this.deviceID = Base64.encodeToString(Hash.sha256(this.deviceSignPubKey.getEncoded()), Base64.URL_SAFE);
            }
            
            // this.deviceEncPubKey = keyStoreWrapper.loadPublicKeyFromFile(config.getEncPubKeyFilename(), "EC");
            this.deviceEncPrivKey = keyStoreWrapper.loadPrivateKeyFromFile(config.getEncPrivKeyFilename(), config.getSignKeystoreAlias(), "EC");
            
            this.serverSignPubKey = keyStoreWrapper.loadPublicKeyFromFile(config.getServerSignPubKeyFilename(), "RSA");
            this.serverEncPubKey = keyStoreWrapper.loadPublicKeyFromFile(config.getServerEncPubKeyFilename(), "EC");
        } catch (GeneralSecurityException | IOException e) {
            throw (new RuntimeException(e));
        }
    }
    
    public void restoreDeviceKeys() {
        try {
            final KeyStoreWrapper keyStoreWrapper = new KeyStoreWrapper(context);
            if (keyStoreWrapper.hasAlias(config.getSignKeystoreAlias())) {
                this.deviceSignPubKey = keyStoreWrapper.getPublicKey(config.getSignKeystoreAlias());
                this.deviceID = Base64.encodeToString(Hash.sha256(this.deviceSignPubKey.getEncoded()), Base64.URL_SAFE);
            }
            
            // this.deviceEncPubKey = keyStoreWrapper.loadPublicKeyFromFile(config.getEncPubKeyFilename(), "EC");
            this.deviceEncPrivKey = keyStoreWrapper.loadPrivateKeyFromFile(config.getEncPrivKeyFilename(), config.getSignKeystoreAlias(), "EC");
        } catch (GeneralSecurityException | IOException e) {
            throw (new RuntimeException(e));
        }
    }

    public void restoreServerKeys() {
        try {
            final KeyStoreWrapper keyStoreWrapper = new KeyStoreWrapper(context);
            this.serverSignPubKey = keyStoreWrapper.loadPublicKeyFromFile(config.getServerSignPubKeyFilename(), "RSA");
            this.serverEncPubKey = keyStoreWrapper.loadPublicKeyFromFile(config.getServerEncPubKeyFilename(), "EC");
        } catch (GeneralSecurityException e) {
            throw (new RuntimeException(e));
        }
    }

    public String encrypt(String clearData) throws GeneralSecurityException {
        byte[] encryptedData = ECIES.encrypt(this.serverEncPubKey, clearData.getBytes());
        return Base64.encodeToString(encryptedData, Base64.DEFAULT);
    }

    public byte[] decrypt(String cipherData) throws GeneralSecurityException {
        byte[] rawData = Base64.decode(cipherData, Base64.DEFAULT);
        return ECIES.decrypt(this.deviceEncPrivKey, rawData);
    }

    public String signData(String data) throws GeneralSecurityException {
        System.out.println("**J**A**V**A** -signData- -- " + data);
        final KeyStoreWrapper keyStoreWrapper = new KeyStoreWrapper(context);
        PrivateKey privateKey = keyStoreWrapper.getPrivateKey(config.getSignKeystoreAlias());
        byte[] signData = RSA.signData(privateKey, data.getBytes());
        return Base64.encodeToString(signData, Base64.DEFAULT);
    }

    public String hashData(String data) throws GeneralSecurityException {
        byte[] hashData = Hash.sha256(data.getBytes());
        return Base64.encodeToString(hashData, Base64.DEFAULT);
    }

    public boolean verifySignature(String data, String signatureData) throws GeneralSecurityException {
        byte[] rawSignature = Base64.decode(signatureData, Base64.DEFAULT);
        return RSA.verifySignature(this.serverSignPubKey, data.getBytes(), rawSignature);
    }

    public boolean verifyDeviceSignature(String data, String signatureData) throws GeneralSecurityException {
        byte[] rawSignature = Base64.decode(signatureData, Base64.DEFAULT);
        return RSA.verifySignature(this.deviceSignPubKey, data.getBytes(), rawSignature);
    }

    public String getDeviceID() {
        return this.deviceID;
    }

    public PublicKey getDeviceSignPubKey() {
        return this.deviceSignPubKey;
    }

    public PrivateKey getDeviceSignPrivKey() throws GeneralSecurityException {        
        final KeyStoreWrapper keyStoreWrapper = new KeyStoreWrapper(context);
        System.out.println("**J**A**V**A** -CryptoSDK- -getDeviceSignPrivKey- ");
        PrivateKey privateKey = keyStoreWrapper.getPrivateKey(config.getSignKeystoreAlias());
        return privateKey;
    }
    
    public PublicKey getDeviceEncPubKey() {
        return this.deviceEncPubKey;
    }

    public PublicKey getServerSignPubKey() {
        return this.serverSignPubKey;
    }

    public PublicKey getServerEncPubKey() {
        return this.serverEncPubKey;
    }

}
