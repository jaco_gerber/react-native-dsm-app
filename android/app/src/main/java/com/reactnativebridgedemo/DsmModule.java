package com.rndsm;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.Callback;
import com.facebook.react.uimanager.IllegalViewOperationException;

import java.security.PublicKey;
import java.security.PrivateKey;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.spec.X509EncodedKeySpec;
import java.security.spec.EncodedKeySpec;
import java.security.spec.EllipticCurve;
import java.security.Security;

import org.bouncycastle.jce.spec.ECNamedCurveParameterSpec;
import org.bouncycastle.jce.spec.ECPublicKeySpec;
import org.bouncycastle.jcajce.provider.asymmetric.util.EC5Util;
import org.bouncycastle.jce.ECPointUtil;
import org.bouncycastle.jce.spec.ECParameterSpec;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.math.ec.ECCurve;
import org.bouncycastle.jce.ECNamedCurveTable;

import java.math.BigInteger;
import java.util.Arrays;

import android.util.Base64;

import mdsm.CryptoSDK;
import mdsm.config.Configuration;

import android.util.Log;

public class DsmModule extends ReactContextBaseJavaModule {
    //constructor
    public static final String REACT_CLASS = "DSM";
    private static ReactApplicationContext reactContext = null;
    private Configuration configuration = new Configuration();
    private static CryptoSDK cryptoSDK = null;

    public DsmModule(ReactApplicationContext context) {
        super(context);
        reactContext = context;
        cryptoSDK = new CryptoSDK(reactContext, configuration);
    }
    
    //Mandatory function getName that specifies the module name
    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @ReactMethod
    public void generateKeys(Callback errorCallback, Callback successCallback) {
        try {
            cryptoSDK.generateKeys();

            successCallback.invoke("generateKeys Success");
        } catch (IllegalViewOperationException e) {
            errorCallback.invoke(e.getMessage());
        }
    }
    
    @ReactMethod
    public void storeServerKeys(String serverSignPubKeyIn, String serverEncPubKeyIn, Callback errorCallback, Callback successCallback) throws GeneralSecurityException {
        try {
            Log.d("storeServerKeys", serverSignPubKeyIn);
            Log.d("storeServerKeys", serverEncPubKeyIn);

            byte[] byteSignPubKey = Base64.decode(serverSignPubKeyIn.getBytes(), Base64.DEFAULT);
            KeyFactory kfRSA = KeyFactory.getInstance("RSA");
            PublicKey serverSignPubKey = kfRSA.generatePublic(new X509EncodedKeySpec(byteSignPubKey));

            byte[] byteEncPubKey = Base64.decode(serverEncPubKeyIn.getBytes(), Base64.DEFAULT);
            Log.d("storeServerKeys", Arrays.toString(byteEncPubKey));
            Log.d("storeServerKeys", "Length :" + String.valueOf(byteEncPubKey.length));
            KeyFactory kfEC = KeyFactory.getInstance("EC");
            ECNamedCurveParameterSpec spec = ECNamedCurveTable.getParameterSpec("secp256k1");
            ECCurve eccCurve = spec.getCurve();
            EllipticCurve ellipticCurve = EC5Util.convertCurve(eccCurve, spec.getSeed());

            java.security.spec.ECPoint point = ECPointUtil.decodePoint(ellipticCurve, byteEncPubKey);
            java.security.spec.ECParameterSpec params = EC5Util.convertSpec(ellipticCurve, spec);
            java.security.spec.ECPublicKeySpec keySpec = new java.security.spec.ECPublicKeySpec(point, params);

            PublicKey serverEncPubKey = kfEC.generatePublic(keySpec);

            
            byte[] keyBytes = serverEncPubKey.getEncoded();
            String strBytes = Base64.encodeToString(keyBytes, Base64.DEFAULT);

            Log.d("storeServerKeys", strBytes);

            cryptoSDK.storeServerKeys(serverSignPubKey, serverEncPubKey);
            successCallback.invoke("Public Keys Stored");
        } catch (IllegalViewOperationException e) {
            errorCallback.invoke(e.getMessage());
        }
    }

    @ReactMethod
    public void restoreKeys(Callback errorCallback, Callback successCallback) {
        try {
            cryptoSDK.restoreKeys();

            successCallback.invoke("All Keys Restored");
        } catch (IllegalViewOperationException e) {
            errorCallback.invoke(e.getMessage());
        }
    }

    @ReactMethod
    public void restoreDeviceKeys(Callback errorCallback, Callback successCallback) {
        try {
            cryptoSDK.restoreDeviceKeys();

            successCallback.invoke("Device Keys Restored");
        } catch (IllegalViewOperationException e) {
            errorCallback.invoke(e.getMessage());
        }
    }

    @ReactMethod
    public void restoreServerKeys(Callback errorCallback, Callback successCallback) {
        try {
            cryptoSDK.restoreServerKeys();

            successCallback.invoke("Server Keys Restored");
        } catch (IllegalViewOperationException e) {
            errorCallback.invoke(e.getMessage());
        }
    }

    @ReactMethod
    public void encrypt(String clearData, Callback errorCallback, Callback successCallback) throws GeneralSecurityException {
        try {
            String cipherData = cryptoSDK.encrypt(clearData);

            successCallback.invoke(cipherData);
        } catch (IllegalViewOperationException e) {
            errorCallback.invoke(e.getMessage());
        }
    }

    @ReactMethod
    public void decrypt(String cipherData, Callback errorCallback, Callback successCallback) throws GeneralSecurityException {
        try {
            byte[] clearData = cryptoSDK.decrypt(cipherData);

            successCallback.invoke(clearData);
        } catch (IllegalViewOperationException e) {
            errorCallback.invoke(e.getMessage());
        }
    }

    @ReactMethod
    public void signData(String data, Callback errorCallback, Callback successCallback) throws GeneralSecurityException {
        try {
            String signedData = cryptoSDK.signData(data);

            successCallback.invoke(signedData);
        } catch (IllegalViewOperationException e) {
            errorCallback.invoke(e.getMessage());
        }
    }

    @ReactMethod
    public void hashData(String data, Callback errorCallback, Callback successCallback) throws GeneralSecurityException {
        try {
            String hashedData = cryptoSDK.hashData(data);

            successCallback.invoke(hashedData);
        } catch (IllegalViewOperationException e) {
            errorCallback.invoke(e.getMessage());
        }
    }

    @ReactMethod
    public void verifySignature(String data, String signatureData, Callback errorCallback, Callback successCallback) throws GeneralSecurityException {
        try {
            boolean verified = cryptoSDK.verifySignature(data, signatureData);

            successCallback.invoke(verified);
        } catch (IllegalViewOperationException e) {
            errorCallback.invoke(e.getMessage());
        }
    }
    
    @ReactMethod
    public void verifyDeviceSignature(String data, String signatureData, Callback errorCallback, Callback successCallback) throws GeneralSecurityException {
        try {
            boolean verified = cryptoSDK.verifyDeviceSignature(data, signatureData);

            successCallback.invoke(verified);
        } catch (IllegalViewOperationException e) {
            errorCallback.invoke(e.getMessage());
        }
    }
    @ReactMethod
    public void getDeviceID(Callback errorCallback, Callback successCallback) {
        try {
            String deviceID = cryptoSDK.getDeviceID();

            successCallback.invoke(deviceID);
        } catch (IllegalViewOperationException e) {
            errorCallback.invoke(e.getMessage());
        }
    }

    
    @ReactMethod
    public void getDeviceSignPrivKey(Callback errorCallback, Callback successCallback) throws GeneralSecurityException {
        try {
            PrivateKey devSignPrivKey = cryptoSDK.getDeviceSignPrivKey();
            
            byte[] keyBytes = devSignPrivKey.getEncoded();
            String strBytes = Base64.encodeToString(keyBytes, Base64.DEFAULT);

            successCallback.invoke(strBytes);
        } catch (IllegalViewOperationException e) {
            errorCallback.invoke(e.getMessage());
        }
    }

    @ReactMethod
    public void getDeviceSignPubKey(Callback errorCallback, Callback successCallback) {
        try {
            PublicKey devSignPubKey = cryptoSDK.getDeviceSignPubKey();
            
            byte[] keyBytes = devSignPubKey.getEncoded();
            String strBytes = Base64.encodeToString(keyBytes, Base64.DEFAULT);

            successCallback.invoke(strBytes);
        } catch (IllegalViewOperationException e) {
            errorCallback.invoke(e.getMessage());
        }
    }

    @ReactMethod
    public void getDeviceEncPubKey(Callback errorCallback, Callback successCallback) {
        try {
            PublicKey devEncPubKey = cryptoSDK.getDeviceEncPubKey();
            byte[] keyBytes = devEncPubKey.getEncoded();
            String strBytes = Base64.encodeToString(keyBytes, Base64.DEFAULT);

            successCallback.invoke(strBytes);
        } catch (IllegalViewOperationException e) {
            errorCallback.invoke(e.getMessage());
        }
    }

    @ReactMethod
    public void getServerSignPubKey(Callback errorCallback, Callback successCallback) {
        try {
            PublicKey serverSignPubKey = cryptoSDK.getServerSignPubKey();
            byte[] keyBytes = serverSignPubKey.getEncoded();
            String strBytes = Base64.encodeToString(keyBytes, Base64.DEFAULT);

            successCallback.invoke(strBytes);
        } catch (IllegalViewOperationException e) {
            errorCallback.invoke(e.getMessage());
        }
    }

    @ReactMethod
    public void getServerEncPubKey(Callback errorCallback, Callback successCallback) {
        try {
            PublicKey ServerEncPubKey = cryptoSDK.getServerEncPubKey();
            byte[] keyBytes = ServerEncPubKey.getEncoded();
            String strBytes = Base64.encodeToString(keyBytes, Base64.DEFAULT);

            successCallback.invoke(strBytes);
        } catch (IllegalViewOperationException e) {
            errorCallback.invoke(e.getMessage());
        }
    }
}